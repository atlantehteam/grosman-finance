<?php get_header(); ?>

<?php $notfound_group = get_field('notfound_group', 'option'); ?>
<main>
    <section id="notFound" class="header-spacing" style="background: url('<?= $notfound_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center bottom;">
        <div class="container">
            <h1 class="xxl-title"><?= $notfound_group['title']; ?></h1>
            <p class="paragraph"><?= $notfound_group['content']; ?></p>
            <a href="<?= home_url(); ?>" class="back-btn">חזרה לעמוד הבית <i class="fas fa-angle-left"></i></a>
        </div>
    </section>
</main>

<?php get_footer(); ?>