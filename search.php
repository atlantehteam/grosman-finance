<?php get_header(); ?>

<?php

$tab_slug = isset($_GET['tab']) ? $_GET['tab'] : null;

$categories = get_categories( array(
	'post_type' => 'videos',
	'orderby' => 'name',
	'order' => 'ASC',
	'taxonomy' => 'videos_categories',
) );

$arguments = array(
	'post_type' => 'videos',
	's' => $_GET['s'],
	'posts_per_page' => 100,
);

$youtube_img_size = wp_is_mobile() ? 'default' : 'sddefault';

$search_posts = new WP_Query( $arguments );
?>

	<?php $videos_group = get_field('videos_group', 'option'); ?>
	<main>
		<section id="videos" class="header-spacing">
			<div class="header-wrapper" style="background: url('<?= $videos_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
				<div class="container">
					<?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
					<h1 class="xxl-title">תוצאות חיפוש</h1>
					<div class="search-wrapper">
						<form action="/" method="get">
							<input type="text" placeholder="חפש וידאו" name="s" id="search" value="<?php the_search_query(); ?>" />
							<button type="submit" class="search-btn">
								<i class="fas fa-arrow-left"></i>
							</button>
						</form>
					</div>
				</div>
			</div>
			<div class="body-wrapper">
				<div class="container">
					<?php if ( $search_posts->have_posts() ) : ?>
						<div class="small-videos-wrapper">
							<div class="row">
								<?php while ( $search_posts->have_posts() ) : $search_posts->the_post(); ?>
									<div class="col-xl-3 col-lg-4 col-md-6">
										<div class="block-wrapper">
											<?php $video_group = get_field('videos_group', get_the_ID()); ?>
											<div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                                <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/$youtube_img_size.jpg"; ?>
                                                <div class="iframe-placeholder-wrapper">
													<img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
												</div>
											</div>
											<div class="text-wrapper">
												<a href="javascript:void(0);" class="video-play-btn">
													<i class="fas fa-play"></i>
												</a>
												<span class="video-text"><?= mb_strimwidth(get_the_title(), 0, 30, '...'); ?></span>
											</div>
										</div>
									</div>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>
	</main>

<?php get_footer(); ?>