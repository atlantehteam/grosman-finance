	<?php get_template_part("elements/footer");?>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/tether.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js?v=<?php echo rand(10,290); ?>"></script>
	
    <?php wp_footer(); ?>

<?php if(!wp_is_mobile()) { ?>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "972584640101", // WhatsApp number
            call_to_action: "שלחו הודעה", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
<?php } ?>

<!-- toggle menu -->
<script type="text/javascript">
// Moved to inline in footer to prevent double click due to speed boosting
jQuery("#menuToggle").click(function() {
    if (!jQuery("#body-wrapper").hasClass("mobile-menu-open")){
        jQuery("html").css("overflow", "hidden");
        jQuery("body").css("overflow", "hidden");
        jQuery("#body-wrapper").addClass("mobile-menu-open");
        jQuery(this).addClass("is-active");
        jQuery("body").addClass("mobile-menu-open");
        jQuery("#mobileMenu").animate({"right":"0px"}, 300);
    } else {
        jQuery("#body-wrapper").removeClass("mobile-menu-open");
        jQuery(this).removeClass("is-active");
        jQuery("body").removeClass("mobile-menu-open");
        jQuery("#mobileMenu").animate({"right":"-220px"}, 300);
        setTimeout(function(){
            jQuery("html").css("overflow", "visible");
            jQuery("body").css("overflow", "visible");
        }, 300);
    }
});
</script>
<!-- /toggle menu -->

</body>
</html>