<?php
/**
 * @package WordPress
 * @subpackage skeleton
 */
?>


<!DOCTYPE html>
<html dir="rtl" lang="he-IL">
<head>
	<meta charset="<?php bloginfo('charset');?>" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head();?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

	<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=<?php echo rand(10,2900); ?>" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/hover-min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/animate-min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/custom.css?v=<?php echo rand(10,2900); ?>" />

	<?php /** Google tag manager here **/?>

	<script type="text/javascript">
		homeUrl = "<?php echo esc_url(home_url('/'));?>";
		themeDir = "<?php bloginfo('template_url'); ?>";
		currentUrl = "<?php echo get_permalink();?>";
	</script>
 <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YNF9V45WR6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-YNF9V45WR6');
</script>
	
	
</head>

<body <?php body_class();?>>

    <?php get_template_part("elements/header");?>
