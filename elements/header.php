<?php
    $menu_group = get_field('menu_group', 'option');
    $menu_list = getProperMenu('header-menu');
    $currentPageID = $post->ID;
    $currentPageURL = get_permalink($currentPageID);
?>

<div id="mobileMenu">
    <div class="header-wrapper">
        <div class="logo-wrapper">
            <img src="<?= $menu_group['logo']['url']; ?>" class="img-fluid" alt="<?= $menu_group['logo']['alt']; ?>">
        </div>
    </div>
    <div class="mobile-list-wrapper">
        <ul class="mobile-list">
            <?php foreach ($menu_list as $item): ?>
                <?php if(isset($item['inner'])) : ?>
                    <li class="item dropdown-item-hover <?php if(($currentPageURL == $item['top']->url)){echo 'active';} ?>">
                        <a href="<?= $item['top']->url; ?>" class="mobile-link"><?= $item['top']->title; ?></a>
                        <a href="javascript:void(0);" class="dropdown-btn">
                            <i class="fas fa-angle-left"></i>
                        </a>
                        <div class="dropdown-list-wrapper">
                            <ul class="dropdown-list">
                                <?php foreach ($item['inner'] as $i => $submenu): ?>
                                    <li class="item <?php if(count($item['inner']) == ($i + 1)) { echo 'last-child'; } ?>">
                                        <a href="<?= $submenu['top']->url; ?>" class="dropdown-link">
                                            <?= $submenu['top']->title; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </li>
                <?php else: ?>
                    <li class="item <?php if(($currentPageURL == $item['top']->url)){echo 'active';} ?>">
                        <a class="mobile-link" href="<?= $item['top']->url; ?>"><?= $item['top']->title; ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php $popup_group = get_field('popup_group', 'option'); ?>
<div id="customPopup">
    <div class="popup-wrapper">
        <a href="javascript:void(0);" class="close-btn">
            <span class="lnr lnr-cross-circle"></span>
        </a>
        <div class="text-wrapper">
            <?= str_replace(['<p>','</p>'], ['',''], $popup_group['title']); ?>
            <small class="small"><?= $popup_group['sub_title']; ?></small>
            <?= do_shortcode($popup_group['shortcode']); ?>
        </div>
        <div class="popup-background-wrapper"></div>
    </div>
</div>

<div id="body-wrapper">
    <header>
        <div id="menu">
            <div class="container">
                <div class="desktop-wrapper">
                    <div class="right-wrapper">
                        <div class="logo-wrapper">
                            <a href="<?= home_url(); ?>" class="logo-link">
                                <img src="<?= $menu_group['logo']['url']; ?>" class="img-fluid" alt="<?= $menu_group['logo']['alt']; ?>">
                            </a>
                        </div>
                    </div>
                    <div class="center-wrapper">
                        <div class="menu-wrapper">
                            <ul class="menu-list">
                                <?php foreach ($menu_list as $item): ?>
                                    <?php if(isset($item['inner'])) : ?>
                                        <li class="item dropdown-hover-wrapper <?php if(($currentPageURL == $item['top']->url)) echo 'active'; ?>">
                                            <a href="<?= $item['top']->url; ?>" class="menu-link"><?= $item['top']->title; ?> <i class="fas fa-angle-down"></i></a>
                                            <div class="dropdown-wrapper">
												<div>
												<?php foreach ($item['inner'] as $i => $submenu): ?>
												<div class="hvr-radial-out <?php if(count($item['inner']) == ($i + 1)) echo 'last-column'; ?>">
													<a href="<?= $submenu['top']->url; ?>" class="dropdown-hover-btn">
														<div class="dropdown-hover-item">
															<?php /* <div class="icon-wrapper">
																<?php $menu_item_group = get_field('menu_item_group', $submenu['top']->ID); ?>
																<img src="<?= $menu_item_group['image']['url']; ?>" class="img-fluid" alt="<?= $menu_item_group['image']['alt']; ?>">
															</div> */ ?>
															<div class="title-wrapper">
																<span class="dropdown-title"><?= $submenu['top']->title; ?></span>
															</div>
														</div>
													</a>
												</div>
												<?php endforeach; ?>
												</div>
                                            </div>
                                        </li>
                                    <?php else: ?>
                                        <li class="item <?php if(($currentPageURL == $item['top']->url)) echo 'active'; ?>">
                                            <a class="menu-link" href="<?= $item['top']->url; ?>"><?= $item['top']->title; ?></a>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="left-wrapper">
                        <a href="javascript:void(0);" class="contact-btn hvr-fade">
                            <b class="bold"><?= $menu_group['popup_button']; ?> <i class="fas fa-angle-left"></i></b>
                        </a>
                        <div class="contact-text-wrapper">
                            <small class="small"><?= $menu_group['phone_title']; ?></small>
                            <strong class="strong"><?= $menu_group['phone_number']; ?></strong>
                        </div>
                    </div>
                </div>
                <div class="mobile-wrapper">
                    <div class="row">
                        <div class="col-4 align-center-column">
                            <div id="menuToggle">
                                <i class="fas fa-bars"></i>
                            </div>
                        </div>
                        <div class="col-8 text-right">
                            <div class="logo-wrapper">
                                <a href="" class="logo-link">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/logo.png" class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>