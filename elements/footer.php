<?php $footer_group = get_field('footer_group', 'option'); ?>
<footer>
    <section id="footer" <?php if((!is_front_page() && is_home()) || (!is_front_page() && !is_home())) echo 'style="margin-top: 20px !important;"'; ?>>
        <div class="information-wrapper">
            <div class="container">
                <div class="row">
                    <?php foreach($footer_group['contact_group']['repeater'] as $repeat) : ?>
                    <div class="col-4">
                        <div class="block-wrapper">
                            <div class="center-block-wrapper">
                                <div class="icon-wrapper">
                                    <img src="<?= $repeat['icon']['url']; ?>" class="img-fluid" alt="<?= $repeat['icon']['alt']; ?>">
                                </div>
                                <div class="text-wrapper">
                                    <small class="small"><?= $repeat['title']; ?></small>
                                    <b class="bold"><?= $repeat['content']; ?></b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2">
                        <div class="row">
                            <div class="col-xl-12 col-6">
                                <div class="logo-wrapper">
                                    <a href="" class="logo-btn">
                                        <img src="<?= $footer_group['content_group']['logo']['url']; ?>" class="img-fluid" alt="<?= $footer_group['content_group']['logo']['alt']; ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-12 col-6">
                                <div class="socials-wrapper">
                                    <?php foreach ($footer_group['content_group']['social_list'] as $social) : ?>
                                    <a href="<?= $social['link']; ?>" target="_blank" class="social-link" rel="nofollow">
                                        <?= $social['icon']; ?>
                                    </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="col-12 desktop-wrapper">
                                <small class="copyright-small"><?= $footer_group['content_group']['rights_text']; ?></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10 desktop-wrapper">
                        <div class="row">
							<?php
								$menu_location = 'footer-menu-1';
								$menu_locations = get_nav_menu_locations();
								$menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
								$menu_name = (isset($menu_object->name) ? $menu_object->name : '');
							?>
                            <div class="col-md-4">
                                <div class="md-title"><?php echo esc_html($menu_name); ?></div>
                                <div class="seperate-line"></div>
								<?php
									wp_nav_menu( array( 'theme_location' => 'footer-menu-1', 'container' => false, 'menu_class' => 'footer-list' ) );
								?>
                            </div>
							<?php
								$menu_location = 'footer-menu-2';
								$menu_locations = get_nav_menu_locations();
								$menu_object = (isset($menu_locations[$menu_location]) ? wp_get_nav_menu_object($menu_locations[$menu_location]) : null);
								$menu_name = (isset($menu_object->name) ? $menu_object->name : '');
							?>
                            <div class="col-md-4">
                                <div class="md-title"><?php echo esc_html($menu_name); ?></div>
                                <div class="seperate-line"></div>
								<?php
									wp_nav_menu( array( 'theme_location' => 'footer-menu-2', 'container' => false, 'menu_class' => 'footer-list' ) );
								?>
                            </div>
                            <div class="col-md-4">
                                <div class="md-title"><?= $footer_group['content_group']['blog_list']['title']; ?></div>
                                <div class="seperate-line"></div>
                                <ul class="articles-list">
                                    <?php foreach ($footer_group['content_group']['blog_list']['articles_list'] as $i => $article) : ?>
                                        <?php $article_group = get_field('article_group', $article->ID); ?>
                                            <li class="item <?php if($i == 0) echo 'first-child'; else if(count($footer_group['content_group']['blog_list']) == ($i + 1)) echo 'last-child'; ?>">
                                                <a href="<?= get_permalink($article); ?>" class="article-link">
                                                    <div class="article-block-wrapper">
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <div class="article-image-wrapper" style="background: url('<?= $article_group['image']['url']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;"></div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="sm-title"><?= mb_strimwidth($article->post_title, 0, 40, '...'); ?></div>
                                                                <div class="date-wrapper">
                                                                    <div class="date-icon-wrapper">
                                                                        <i class="fas fa-calendar-alt"></i>
                                                                    </div>
                                                                    <small class="small-date"><?= get_the_date('d.m.Y', $article->ID); ?></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="development-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-sm-5 order-sm-0 order-1 mobile-wrapper text-sm-left">
                        <small class="copyright-small"><?= $footer_group['content_group']['rights_text']; ?></small>
                    </div>
                    <div class="col-xl-12 col-sm-7 order-sm-1 order-0 text-sm-center text-right">
                        <span class="development-text">
                            <a href="<?= $footer_group['credits_group']['development_link']; ?>" class="development-link">בניית אתר ועיצוב</a> | <a href="<?= $footer_group['credits_group']['seo_link']; ?>" class="development-link">קידום אתרים ניר אזולאי</a>
                        </span>
                        <div class="logo-wrapper">
                            <img src="<?= $footer_group['credits_group']['company_logo']['url']; ?>" class="img-fluid" alt="<?= $footer_group['credits_group']['company_logo']['alt']; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
</div>

<div id="scrollTop">
    <i class="fas fa-chevron-up"></i>
</div>

<?php $fixed_buttons_group = get_field('fixed_buttons_group', 'option'); ?>
<div id="fixedButtons">
    <div class="container-fluid">
        <div class="row">
            <?php foreach ($fixed_buttons_group['repeater'] as $i => $button) : ?>
            <div class="col-4 <?php if($i == 1) echo 'center-column'; ?>">
                <a href="<?= $button['link']; ?>" class="fixed-btn">
                    <div class="block-wrapper">
                        <div class="icon-wrapper">
                            <?= $button['icon']; ?>
                        </div>
                        <div class="text-wrapper">
                            <small class="small"><?= $button['title']; ?></small>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>