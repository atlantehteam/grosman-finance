<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>

    <main>
        <section id="page" class="header-spacing service-wrapper">
            <div class="container">
                <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                <?php $services_page_group = get_field('services_page_group'); ?>
                <div class="body-wrapper">
                    <div class="content-wrapper">
                        <div class="service-header-wrapper" style="background: url('<?= $services_page_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                            <h1 class="page-title"><?= $services_page_group['title']; ?></h1>
                        </div>
                        <div class="content-text-wrapper">
                            <?= the_content(); ?>
                            <?= do_shortcode('[contact]'); ?>
                        </div>
                    </div>
                    <div class="side-wrapper">
                        <?php dynamic_sidebar("single-sidebar"); ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>