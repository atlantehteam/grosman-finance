<?php get_header(); ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
    'posts_per_page' => 12,
    'order'=> 'DESC',
    'paged'=> $paged,
    'post_type' => 'post',
);
$posts_query = new WP_Query( $args );
?>
<!-- home.php is template for posts index (archive of native Post post type, which is a special case in WP). WP will attempt to look it up for index of posts, whether they are displayed at the root of the site or at dedicated posts page. -->
    <main>
        <section id="page" class="header-spacing archive-wrapper">
            <?php $blog_group = get_field('blog_group', 'option'); ?>
            <div class="header-wrapper" style="background: url('<?= $blog_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                <div class="container">
                    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                    <h1 class="xxl-title"><?= $blog_group['title']; ?></h1>
                </div>
            </div>
            <div class="body-wrapper">
                <div class="container">
                    <div class="content-wrapper">
                        <div class="articles-wrapper">
                            <div class="row">
                                <?php if($posts_query->have_posts()) : ?>
                                    <?php while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="block-wrapper">
                                                <?php $article_group = get_field('article_group', get_the_ID()); ?>
                                                <div class="image-wrapper" style="background: url('<?= $article_group['image']['url']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;"></div>
                                                <div class="text-wrapper">
                                                    <h2 class="xl-title"><?= the_title(); ?></h2>
                                                    <span class="date">
                                                <i class="fas fa-calendar-alt"></i>
                                                <span class="text"><?= get_the_date(); ?></span>
                                            </span>
                                                    <p class="paragraph"><?= strip_tags(limitTextChars(get_the_content(), 150, false, true)); ?></p>
                                                    <a href="<?= the_permalink(); ?>" class="read-btn">
                                                        קרא עוד <i class="fas fa-angle-left"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                        <div id="navigation">
                            <?php
                            $GLOBALS['wp_query']->max_num_pages = $posts_query->max_num_pages;

                            the_posts_pagination( array(
                                'prev_text' => '<span><i class="fas fa-angle-right"></i>הקודם</span>',
                                'next_text' => '<span>הבא<i class="fas fa-angle-left"></i></span>',
                                'screen_reader_text' => __( 'Posts navigation' )
                            ) );
                            ?>
                        </div>
                    </div>
                    <div class="side-wrapper">
                        <?php dynamic_sidebar("single-sidebar"); ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>