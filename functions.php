<?php

/* REGISTER SIDEBARS */
function register_my_widgets(){

    register_sidebar( array(
        'name' => "Articles / Categories Sidebar",
        'id' => 'single-sidebar',
        'description' => 'Side Bar',
        'before_widget' => '',
        'after_widget' => '',
        "before_title" => '<div class="widgettitle">',
        "after_title" => '</div>'
    ) );

}

add_action( 'widgets_init', 'register_my_widgets' );

/* CREATE WIDGET */
class aside_contact_form_widget extends WP_Widget {

    function __construct() {
        parent::__construct(

        // Base ID of your widget
            'aside_contact_form_widget',

            // Widget name will appear in UI
            'Contact Widget - יצירת קשר',

            // Widget description
            array( 'description' =>  'Widget for displaying Contact Form')
        );
    }

    // Creating widget front-end

    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $contact_widget_group = get_field('contact_widget_group', 'widget_' . $args['widget_id']);

        ?>

        <div class="side-title-wrapper mobile-wrapper">
            <div class="side-title"><?= $title; ?></div>
        </div>
        <div class="contact-widget-wrapper">
            <div class="text-wrapper">
                <?= str_replace(['<p>','</p>'], ['',''], $contact_widget_group['title']); ?>
                <strong class="strong"><?= $contact_widget_group['sub_title']; ?></strong>
            </div>
            <div class="number-wrapper">
                <div class="arrow-wrapper right-arrow">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/arrows-gray.gif" class="img-fluid" alt="">
                </div>
                <div class="phone-wrapper">
                    <i class="fas fa-phone"></i>
                    <b class="bold"><?= $contact_widget_group['phone_number']; ?></b>
                </div>
                <div class="arrow-wrapper left-arrow">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/arrows-gray2.gif" class="img-fluid" alt="">
                </div>
            </div>
            <div class="left-wrapper">
                <small class="small"><?= $contact_widget_group['form_title']; ?></small>
                <?= do_shortcode($contact_widget_group['shortcode']); ?>
            </div>
        </div>

        <?php
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}

register_widget( 'aside_contact_form_widget' );

class aside_services_list_widget extends WP_Widget {

    function __construct() {
        parent::__construct(

        // Base ID of your widget
            'aside_services_list_widget',

            // Widget name will appear in UI
            'Services List Widget - רשימת שירותים',

            // Widget description
            array( 'description' =>  'Widget for displaying Services')
        );
    }

    // Creating widget front-end

    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $services_list_widget_group = get_field('services_list_widget_group', 'widget_' . $args['widget_id']);

        ?>

        <div class="side-title-wrapper">
            <div class="side-title"><?= $title; ?></div>
        </div>
        <div class="services-widget-wrapper">
            <?php foreach($services_list_widget_group['services_list'] as $service) : ?>
                <a href="<?= $service['link']; ?>" class="information-btn-wrapper">
                    <div class="block-wrapper">
                        <div class="block-icon-wrapper">
                            <img src="<?= $service['icon']['url']; ?>" class="img-fluid" alt="<?= $service['icon']['alt']; ?>">
                        </div>
                        <div class="block-text-wrapper">
                            <div class="lg-title"><?= $service['title']; ?></div>
                            <span class="information-btn"><i class="fas fa-angle-left"></i></span>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>

        <?php
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}

register_widget( 'aside_services_list_widget' );

class aside_recommendations_list_widget extends WP_Widget {

    function __construct() {
        parent::__construct(

        // Base ID of your widget
            'aside_recommendations_list_widget',

            // Widget name will appear in UI
            'Recommendations List Widget - רשימת המלצות',

            // Widget description
            array( 'description' =>  'Widget for displaying Recommendations')
        );
    }

    // Creating widget front-end

    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $recommendations_group = get_field('recommendations_group', 'option');

        ?>

        <div class="side-title-wrapper">
            <div class="side-title"><?= $recommendations_group['title']; ?></div>
        </div>
        <div class="recommendations-widget-wrapper">
            <div class="recommendations-wrapper">
                <div id="owl-recommendations" class="owl-carousel owl-theme owl-loaded owl-drag owl-rtl">
                    <?php foreach ($recommendations_group['recommendations_list'] as $item) : ?>
                    <div class="item">
                        <div class="block-wrapper">
                            <div class="quote-wrapper text-right">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/quote.png" class="img-fluid" alt="לקוח ממליץ">
                            </div>
                            <?php if(strlen($item['content']) <= 280) : ?>
                                <p class="paragraph" style="margin-bottom: 20px;"><?= strip_tags($item['content']); ?></p>
                            <?php else: ?>
                                <p class="paragraph"><?= mb_strimwidth(strip_tags($item['content']), 0, 180, '<span class="three-dots">...</span>'); ?></p>
                                <p class="paragraph hide-paragraph"><?= mb_strimwidth(strip_tags($item['content']), 180, strlen($item->post_content)); ?></p>
                                <span class="read-more-btn">קרא עוד</span>
                            <?php endif; ?>
                            <div class="seperate-line"></div>
                            <div class="profile-wrapper">
                                <div class="profile-text-wrapper">
                                    <div class="stars-wrapper">
                                        <?php for($i = 0; $i < $item['rating']; $i++) : ?>
                                            <div class="star">
                                                <i class="fas fa-star"></i>
                                            </div>
                                        <?php endfor; ?>
                                    </div>
                                    <small class="profile-name"><?= $item['name']; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="navigation-wrapper">
                    <div class="arrow-wrapper right-arrow-wrapper">
                        <i class="fas fa-chevron-right"></i>
                    </div>
                    <div class="dots-wrapper">
                        <ul class="owl-dots">
                            <li class="owl-dot"></li>
                            <li class="owl-dot"></li>
                            <li class="owl-dot"></li>
                            <li class="owl-dot"></li>
                            <li class="owl-dot"></li>
                        </ul>
                    </div>
                    <div class="arrow-wrapper left-arrow-wrapper">
                        <i class="fas fa-chevron-left"></i>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}

register_widget( 'aside_recommendations_list_widget' );

/* Shortcode Article & Category */
function shortcode_contact_form() {
    $output = ' ';
    $className = null;
    $shortcode_contact_field = get_field('shortcode_contact_form_group', 'option');
    if($shortcode_contact_field['toggle'] === true) {
        if(!empty($shortcode_contact_field['title'])) {
            $output .= '<div class="shortcode-contact-wrapper">';
            $output .= '<div class="row">';
            $output .= '<div class="col-xl-2 flex-center-column">';
            $output .= '<div class="profile-wrapper">';
            $output .= '<img src="' . $shortcode_contact_field['profile_image']['url'] . '" class="img-fluid" alt="' . $shortcode_contact_field['profile_image']['alt'] . '">';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<div class="col-xl-10">';
            $output .= str_replace(['<p>','</p>'], ['',''], $shortcode_contact_field['title']);
            $output .= '<div class="number-wrapper">';
            $output .= '<div class="arrow-wrapper left-arrow">';
            $output .= '<img src="' . get_bloginfo('template_url') . '/assets/images/desktop/arrows-gray.gif" class="img-fluid" alt="">';
            $output .= '</div>';
            $output .= '<div class="phone-wrapper">';
            $output .= '<i class="fas fa-phone"></i>';
            $output .= '<b class="bold">072-3954389</b>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<small class="contact-small">' . $shortcode_contact_field['form_title'] . '</small>';
            $output .= str_replace(['<p>','</p>'], ['',''], do_shortcode($shortcode_contact_field['shortcode']));
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
    }
    return $output;
}

add_shortcode('contact', 'shortcode_contact_form');

function limitTextWords($content = false, $limit = false, $stripTags = false, $ellipsis = false)
{
    if ($content && $limit) {
        $content = ($stripTags ? strip_tags($content) : $content);
        $content = explode(' ', $content, $limit+1);
        array_pop($content);
        if ($ellipsis) {
            array_push($content, '...');
        }
        $content = implode(' ', $content);
    }
    return $content;
}

function limitTextChars($content = false, $limit = false, $stripTags = false, $ellipsis = false)
{
    if ($content && $limit) {
        $content  = ($stripTags ? strip_tags($content) : $content);
        $ellipsis = ($ellipsis ? "..." : $ellipsis);
        $content  = mb_strimwidth($content, 0, $limit, $ellipsis);
    }
    return $content;
}


function is_ajax(){
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
}

if( function_exists('acf_add_options_page') ) {
 
    $option_page = acf_add_options_page(array(
		'page_title' 	=> 'הגדרות כלליות',
        'menu_title' 	=> 'הגדרות כלליות',
        'slug'          => 'optional-page'
	));
 
}


function register_my_menus() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu' ),
			'footer-menu-top' => __( 'Footer Menu Top' ),
			'footer-menu-bottom' => __( 'Footer Menu Bottom' ),
			'footer-menu-1' => __( 'Footer Menu 1' ),
			'footer-menu-2' => __( 'Footer Menu 2' ),
		)
	);
}
add_action( 'init', 'register_my_menus' );

function getProperMenu($menu_name){
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object($locations[$menu_name]);
	return getMenuById($menu->term_id);
}
function getMenuById($menu_id){
	$menuitems = wp_get_nav_menu_items($menu_id);
	if(empty($menuitems)){
		$menuitems = array();
	}
	//print_r($menuitems);die();
	$tree = buildTree($menuitems);
	return $tree;
}
function buildTree($data, $parent = 0) {
	$tree = array();
	foreach ($data as $d):
		if ($d->menu_item_parent == $parent) {
			$children = buildTree($data,$d->ID);
			if(!empty($children)){
				$tree[$d->ID]['inner'] = $children;
			}
			$tree[$d->ID]['top'] = $d;
		}
	endforeach;
	
	return $tree;
}



add_shortcode('breadcrumbs', 'putBreadcrumbs');

function putBreadcrumbs(){
	if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div id="gbreadcrumbs">','</div>
');
}
}


/* Most Popular posts */
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}



/**
 * Related posts
 * 
 * @global object $post
 * @param array $args
 * @return
 */
function wcr_related_posts($args = array()) {
    global $post;

    // default args
    $args = wp_parse_args($args, array(
        'post_id' => !empty($post) ? $post->ID : '',
        'taxonomy' => 'category',
        'limit' => 3,
        'post_type' => !empty($post) ? $post->post_type : 'post',
        'orderby' => 'date',
        'order' => 'DESC'
    ));

    // check taxonomy
    if (!taxonomy_exists($args['taxonomy'])) {
        return;
    }

    // post taxonomies
    $taxonomies = wp_get_post_terms($args['post_id'], $args['taxonomy'], array('fields' => 'ids'));

    if (empty($taxonomies)) {
        return;
    }

    // query
    $related_posts = get_posts(array(
        'post__not_in' => (array) $args['post_id'],
        'post_type' => $args['post_type'],
        'tax_query' => array(
            array(
                'taxonomy' => $args['taxonomy'],
                'field' => 'term_id',
                'terms' => $taxonomies
            ),
        ),
        'posts_per_page' => $args['limit'],
        'orderby' => $args['orderby'],
        'order' => $args['order']
    ));

    include( locate_template('elements/related-posts-remplate.php', false, false) );

    wp_reset_postdata();
}



/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


// add default image setting to ACF image fields
  // let's you select a defualt image
  // this is simply taking advantage of a field setting that already exists
  
	add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field');
	function add_default_value_to_image_field($field) {
		acf_render_field_setting( $field, array(
			'label'			=> 'Default Image',
			'instructions'		=> 'Appears when creating a new post',
			'type'			=> 'image',
			'name'			=> 'default_value',
		));
	}


/***************************** Sacha Code **********************************/

/**
 * Redirect to the homepage all users trying to access feeds.
 */
function disable_feeds() {
	wp_redirect( home_url() );
	die;
}

// Disable global RSS, RDF & Atom feeds.
add_action( 'do_feed',      'disable_feeds', -1 );
add_action( 'do_feed_rdf',  'disable_feeds', -1 );
add_action( 'do_feed_rss',  'disable_feeds', -1 );
add_action( 'do_feed_rss2', 'disable_feeds', -1 );
add_action( 'do_feed_atom', 'disable_feeds', -1 );

// Disable comment feeds.
add_action( 'do_feed_rss2_comments', 'disable_feeds', -1 );
add_action( 'do_feed_atom_comments', 'disable_feeds', -1 );

// Prevent feed links from being inserted in the <head> of the page.
add_action( 'feed_links_show_posts_feed',    '__return_false', -1 );
add_action( 'feed_links_show_comments_feed', '__return_false', -1 );
remove_action( 'wp_head', 'feed_links',       2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );

remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');

function crunchify_cleanup_query_string( $src ){ 
	$parts = explode( '?', $src ); 
	return $parts[0]; 
} 
add_filter( 'script_loader_src', 'crunchify_cleanup_query_string', 15, 1 ); 
add_filter( 'style_loader_src', 'crunchify_cleanup_query_string', 15, 1 );

remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action('rest_api_init', 'wp_oembed_register_route');
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

/***************************** END Sacha Code **********************************/

?>