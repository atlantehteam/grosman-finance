(function($) {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
    
        if (scroll >= 50) {
            $("#scrollTop").fadeIn();
        } else {
            $("#scrollTop").fadeOut();
        }
    });
    
    $(document).ready(function ($) {
        $('.popup-youtube').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    
        /* YOUTUBE IFRAME EMBED */
        $( ".iframe-wrapper" ).each(function( index ) {
            var elm = $(this);
            $(this).parent().find('.video-play-btn').on("click", function(e) {
                e.preventDefault();
                elm.find('.iframe').remove();
                elm.append('<iframe class="iframe"></iframe>');
                var iframe = elm.find('.iframe');
                iframe.attr( "frameborder", "0" );
                iframe.attr( "allowfullscreen", "" );
                iframe.attr( "src", "https://www.youtube.com/embed/" + elm.attr('data-embed') + "?rel=0&showinfo=0&autoplay=1" );
                elm.find('.iframe-placeholder-wrapper').fadeOut();
            });
        });
    });
    
    $(function(){
    
        $("#menu .desktop-wrapper .contact-btn, #fixedButtons .center-column").click(function (e) {
            e.preventDefault();
            $("#customPopup")
                .css("display", "flex")
                .hide()
                .fadeIn();
        });
    
        var mouseX = 0;
        var mouseY = 0;
        var popupCounter = 0;
        document.addEventListener("mousemove", function(e) {
            mouseX = e.clientX;
            mouseY = e.clientY;
            $('#coordinates').html("<br />X: " + e.clientX + "px<br />Y: " + e.clientY + "px");
        });
    
        const popupTriggerKey = 'popup_last_triggered';
        const quietTime = 24 * 60 * 60 * 1000 // 24 hours
        const lastTriggered = parseInt(sessionStorage.getItem(popupTriggerKey) || 0);
        function onMouseLeave(msg, myYes) {
            if (mouseY < 50) {
                var display = $("#customPopup").css('display');
                if (display != 'flex') {
                        $("#customPopup")
                        .css("display", "flex")
                        .hide()
                        .fadeIn();
                    sessionStorage.setItem('popup_last_triggered', Date.now());
                    $(document).unbind('mouseleave', onMouseLeave)
                }
            }
        }

        const isLoggedIn = $('body.logged-in').length;
        if (!isLoggedIn && (lastTriggered + quietTime < Date.now())) {
            $(document).mouseleave(onMouseLeave)
        }
    
    
    
    
        $("#customPopup .close-btn").click(function (e) {
            e.preventDefault();
            $("#customPopup").fadeOut();
        });
    
        $("#scrollTop").click(function() {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });
    
        var slider = $('#owl-slider, .owl-slider, .owl-carousel').owlCarousel({
            loop: true,
            items: 3,
            margin: 20,
            rtl: true,
            nav: false,
            dots: true,
            dotsContainer: '#recommendations .recommendations-wrapper .navigation-wrapper .dots-wrapper .owl-dots',
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1200:{
                    items:3
                }
            }
        });
    
        slider.on('changed.owl.carousel', function(e) { /* Fixes - Autoplay timer not reset when using nav dots */
            slider.trigger('stop.owl.autoplay');
            slider.trigger('play.owl.autoplay');
        });
    
        $('#recommendations .recommendations-wrapper .navigation-wrapper .right-arrow-wrapper').click(function() {
            slider.trigger('prev.owl.carousel', [300]);
        });
    
        $('#recommendations .recommendations-wrapper .navigation-wrapper .left-arrow-wrapper').click(function() {
            slider.trigger('next.owl.carousel', [300]);
        });
    
        /* RECOMMENDATIONS SLIDER */
        var recommendations = $('#owl-recommendations').owlCarousel({
            loop: true,
            items: 1,
            rtl: true,
            nav: false,
            dots: true,
            dotsContainer: '#page .body-wrapper .side-wrapper .recommendations-widget-wrapper .recommendations-wrapper .navigation-wrapper .dots-wrapper .owl-dots',
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
        });
    
        recommendations.on('changed.owl.carousel', function(e) { /* Fixes - Autoplay timer not reset when using nav dots */
            recommendations.trigger('stop.owl.autoplay');
            recommendations.trigger('play.owl.autoplay');
        });
    
        $('#page .body-wrapper .side-wrapper .recommendations-widget-wrapper .recommendations-wrapper .navigation-wrapper .right-arrow-wrapper').click(function() {
            recommendations.trigger('prev.owl.carousel', [300]);
        });
    
        $('#page .body-wrapper .side-wrapper .recommendations-widget-wrapper .recommendations-wrapper .navigation-wrapper .left-arrow-wrapper').click(function() {
            recommendations.trigger('next.owl.carousel', [300]);
        });
    
        /* PARALLAX SLIDER */
        var parallax = $('#owl-parallax').owlCarousel({
            loop: true,
            items: 3,
            margin: 0,
            center: true,
            rtl: true,
            nav: false,
            responsiveClass:true,
            responsive:{
                0:{
                    items: 1,
                    center: false
                },
                991:{
                    items: 3,
                    center: true
                }
            }
        });
    
        parallax.on('changed.owl.carousel', function(event) {
            $('.iframe-wrapper .iframe').remove();
            $('.iframe-wrapper .iframe-placeholder-wrapper').css('opacity', 1);
            $('.iframe-wrapper .iframe-placeholder-wrapper').css('display', 'block');
        })
    
        $('#parallax .slider-wrapper .owl-arrow-right').click(function() {
            parallax.trigger('prev.owl.carousel', [300]);
        });
    
        $('#parallax .slider-wrapper .owl-arrow-left').click(function() {
            parallax.trigger('next.owl.carousel', [300]);
        });
    
        // Moved to inline in footer to prevent double click due to speed boosting
        // $("#menuToggle").click(function() {
        //     if (!$("#body-wrapper").hasClass("mobile-menu-open")){
        //         $("html").css("overflow", "hidden");
        //         $("body").css("overflow", "hidden");
        //         $("#body-wrapper").addClass("mobile-menu-open");
        //         $(this).addClass("is-active");
        //         $("body").addClass("mobile-menu-open");
        //         $("#mobileMenu").animate({"right":"0px"}, 300);
        //     } else {
        //         $("#body-wrapper").removeClass("mobile-menu-open");
        //         $(this).removeClass("is-active");
        //         $("body").removeClass("mobile-menu-open");
        //         $("#mobileMenu").animate({"right":"-220px"}, 300);
        //         setTimeout(function(){
        //             $("html").css("overflow", "visible");
        //             $("body").css("overflow", "visible");
        //         }, 300);
        //     }
        // });
    
        $('.recommendations-wrapper .block-wrapper .read-more-btn').click(function() {
           const parent = $(this).parent();
           if(parent.find('.hide-paragraph').is(":visible")){
               parent.find('.hide-paragraph').css("display", "inline")
                   .hide()
                   .fadeOut();
               parent.find('.three-dots').css("display", "inline")
                   .hide()
                   .fadeIn();
               $(this).text('קרא עוד');
           } else {
               parent.find('.hide-paragraph').css("display", "inline")
                   .hide()
                   .fadeIn();
               parent.find('.three-dots').css("display", "inline")
                   .hide()
                   .fadeOut();
               $(this).text('הצג פחות');
           }
        });
    
        $("#mobileMenu .dropdown-btn").click(function() {
            if (!$(this).hasClass("is-active")){
                $(this).addClass("is-active");
                $(this).parent().find('.dropdown-list-wrapper').fadeIn("slow");
            } else {
                $(this).removeClass("is-active");
                $(this).parent().find('.dropdown-list-wrapper').fadeOut("slow");
            }
        });
    });
})(jQuery)
