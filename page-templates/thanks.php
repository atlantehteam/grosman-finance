<?php
/*
Template Name: Thanks
@package WordPress
@subpackage skeleton
 */
?>
<?php get_header(); ?>

<?php $thanks_group = get_field('thanks_group'); ?>
<main>
    <section id="notFound" class="header-spacing" style="background: url('<?= $thanks_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center bottom;">
        <div class="container">
            <h1 class="xxl-title"><?= $thanks_group['title']; ?></h1>
            <p class="paragraph"><?= $thanks_group['content']; ?></p>
            <a href="<?= home_url(); ?>" class="back-btn">חזרה לעמוד הבית <i class="fas fa-angle-left"></i></a>
        </div>
    </section>
</main>

<?php get_footer(); ?>