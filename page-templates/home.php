<?php
/*
Template Name: Home
@package WordPress
@subpackage skeleton
 */
?>
<?php get_header(); ?>

    <main>
        <?php $header_group = get_field('header_group'); ?>
        <section id="header" class="header-spacing">
            <div class="mobile-wrapper">
                <div class="airplane-mobile-wrapper animated fadeIn delay-2s"></div>
                <div class="building-mobile-wrapper animated fadeIn delay-1s"></div>
                <div class="container">
                    <div class="person-wrapper">
                        <div class="quote-wrapper">
                            <a class="play-btn popup-youtube" href="<?= $header_group['video_group']['button_link']; ?>">
                                <span class="absolute-text"><?= $header_group['video_group']['button_title']; ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="desktop-wrapper">
                <div class="airplane-wrapper animated fadeIn delay-3s"></div>
                <div class="left-building-wrapper animated fadeIn delay-1s"></div>
                <div class="right-building-wrapper animated fadeIn delay-2s"></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="header-wrapper">
                            <h1 class="xxl-title"><?= $header_group['content_group']['title']; ?></h1>
                            <h2 class="xl-title"><?= $header_group['content_group']['sub_title']; ?></h2>
                            <u class="underline-title"><?= $header_group['content_group']['underline_title']; ?></u>
                            <ul class="advantages-list">
                                <?php foreach ($header_group['content_group']['advantages_list'] as $item) : ?>
                                <li class="item">
                                    <?= str_replace(['<p>','</p>'], ['',''], $item['advantage']); ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="relationship-wrapper desktop-wrapper">
                                <img src="<?= $header_group['content_group']['relationship_image']['url']; ?>" class="img-fluid" alt="<?= $header_group['content_group']['relationship_image']['alt']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="person-wrapper desktop-inline-wrapper">
                            <div class="quote-wrapper">
                                <a class="play-btn popup-youtube" href="<?= $header_group['video_group']['button_link']; ?>">
                                    <span class="absolute-text"><?= $header_group['video_group']['button_title']; ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="side-wrapper">
                            <h4 class="md-title"><?= $header_group['services_group']['title']; ?></h4>
                            <?php foreach($header_group['services_group']['services_list'] as $service) : ?>
                                <a href="<?= $service['link']; ?>" class="information-btn-wrapper">
                                    <div class="block-wrapper">
                                        <div class="block-icon-wrapper">
                                            <img src="<?= $service['icon']['url']; ?>" class="img-fluid" alt="<?= $service['icon']['alt']; ?>">
                                        </div>
                                        <div class="block-text-wrapper">
                                            <h3 class="lg-title"><?= $service['title']; ?></h3>
                                            <span class="information-btn"><span class="desktop-inline-wrapper">לפרטים</span> <i class="fas fa-angle-left"></i></span>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $contact_group = get_field('contact_group'); ?>
        <section id="longContact">
            <div class="container">
                <div class="contact-wrapper">
                    <div class="right-wrapper">
                        <div class="text-wrapper">
                            <?= str_replace(['<p>','</p>'], ['',''], $contact_group['title']); ?>
                            <strong class="strong"><?= $contact_group['sub_title']; ?></strong>
                        </div>
                        <div class="number-wrapper">
                            <div class="arrows-wrapper desktop-inline-wrapper">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/arrows.gif" class="img-fluid" alt="">
                            </div>
                            <div class="mobile-inline-wrapper">
                                <div class="arrow-wrapper right-arrow">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/arrows-gray.gif" class="img-fluid" alt="">
                                </div>
                            </div>
                            <div class="phone-wrapper">
                                <i class="fas fa-phone"></i>
                                <b class="bold"><?= $contact_group['phone_number']; ?></b>
                            </div>
                            <div class="mobile-inline-wrapper">
                                <div class="arrow-wrapper left-arrow">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/arrows-gray2.gif" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="left-wrapper">
                        <small class="small"><?= $contact_group['form_title']; ?></small>
                        <?= do_shortcode($contact_group['shortcode']); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php $top_content_group = get_field('top_content_group'); ?>
        <section class="mortgages-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 flex-center-column">
                        <div class="text-wrapper">
                            <h2 class="xl-title"><?= $top_content_group['title']; ?></h2>
                            <?= $top_content_group['content']; ?>
                        </div>
                    </div>
                    <div class="col-lg-7 absolute-image-column flex-center-column">
                        <div class="absolute-image-wrapper man-block-image"></div>
                        <div class="absolute-image-wrapper calculator-block-image"></div>
                        <div class="side-wrapper left-side-wrapper">
                            <h3 class="lg-title"><?= str_replace(['<p>','</p>'], ['',''], $top_content_group['block_group']['title']); ?></h3>
                            <ul class="side-list">
                                <?php foreach ($top_content_group['block_group']['advantages_list'] as $i => $advantage) : ?>
                                <li class="item <?php if(count($top_content_group['block_group']['advantages_list']) == ($i + 1)) echo 'last-child'; ?>">
                                    <div class="icon-wrapper">
                                        <img src="<?= $advantage['icon']['url']; ?>" class="img-fluid" alt="<?= $advantage['icon']['alt']; ?>">
                                    </div>
                                    <span class="text"><?= $advantage['title']; ?></span>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                            <a href="<?= $top_content_group['block_group']['button_link']; ?>" class="mortgage-btn"><?= $top_content_group['block_group']['button_title']; ?> <i class="fas fa-angle-left"></i></a>
                        </div>
                        <div class="mobile-side-image-wrapper text-center">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/calculator.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
            $videos_group = get_field('videos_group');
            $youtube_img_size = wp_is_mobile() ? 'default' : 'sddefault';
        ?>
        <section id="parallax">
            <div class="container">
                <h2 class="xl-title"><?= $videos_group['title']; ?></h2>
                <div class="slider-wrapper">
                    <div id="owl-parallax" class="owl-carousel owl-theme owl-loaded owl-drag owl-rtl">
                        <?php foreach($videos_group['videos_list'] as $video) : ?>
                        <?php $video_group = get_field('videos_group', $video->ID); ?>
                        <div class="item">
                            <div class="block-wrapper">
                                <div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                    <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/$youtube_img_size.jpg"; ?>
                                    <div class="iframe-placeholder-wrapper">
                                        <img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
                                    </div>
                                </div>
                                <div class="text-wrapper">
                                    <a href="javascript:void(0);" class="video-play-btn">
                                        <i class="fas fa-play"></i>
                                    </a>
                                    <span class="video-text"><?= mb_strimwidth($video->post_title, 0, 30, '...'); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <a href="javascript:void(0);" class="owl-arrow-right">
                        <i class="fas fa-angle-right"></i>
                    </a>
                    <a href="javascript:void(0);" class="owl-arrow-left">
                        <i class="fas fa-angle-left"></i>
                    </a>
                </div>
                <a href="<?= $videos_group['button_link']; ?>" class="videos-btn"><?= $videos_group['button_title']; ?> <i class="fas fa-angle-left"></i></a>
            </div>
        </section>

        <?php $about_group = get_field('about_group'); ?>
        <section id="about">
            <div class="container">
                <div class="about-wrapper">
                    <div class="row">
                        <div class="col-xl-2 column-content-flex-center profile-column">
                            <div class="flex-center-wrapper">
                                <div class="profile-wrapper text-center">
                                    <img src="<?= $about_group['profile_image']['url']; ?>" class="img-fluid" alt="<?= $about_group['profile_image']['alt']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 column-content-flex-center border-column">
                            <div class="flex-center-wrapper">
                                <div class="mobile-profile-wrapper">
                                    <div class="profile-wrapper text-center inline-profile-wrapper">
                                        <img src="<?= $about_group['profile_image']['url']; ?>" class="img-fluid" alt="<?= $about_group['profile_image']['alt']; ?>">
                                    </div>
                                    <div class="inline-title-wrapper">
                                        <?= $about_group['meet_title']; ?>
                                    </div>
                                </div>
                                <ul class="about-list">
                                    <?php foreach($about_group['meet_list'] as $list) : ?>
                                    <li class="item">
                                        <span class="list-text"><?= $list['meet']; ?></span>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-4 column-content-flex-center left-column">
                            <div class="flex-center-wrapper">
                                <h3 class="lg-title"><?= $about_group['about_title']; ?></h3>
                                <span class="sub-title"><?= $about_group['about_sub_title']; ?></span>
                                <div class="values-wrapper">
                                    <div class="row">
                                        <?php foreach($about_group['about_list'] as $i => $list) : ?>
                                            <div class="<?php if((($i + 1) % 2) == 0) echo 'col-8'; else echo 'col-4'; ?>">
                                                <div class="value-wrapper">
                                                    <span class="value-text"><?= $list['about']; ?></span>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $bottom_content_group = get_field('bottom_content_group'); ?>
        <section class="mortgages-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 order-lg-0 order-1 absolute-image-column flex-center-column">
                        <div class="absolute-image-wrapper woman-block-image"></div>
                        <div class="absolute-image-wrapper plant-block-image"></div>
                        <div class="side-wrapper right-side-wrapper">
                            <h3 class="lg-title"><?= str_replace(['<p>','</p>'], ['',''], $bottom_content_group['block_group']['title']); ?></h3>
                            <ul class="side-list">
                                <?php foreach ($bottom_content_group['block_group']['advantages_list'] as $i => $advantage) : ?>
                                    <li class="item <?php if(count($bottom_content_group['block_group']['advantages_list']) == ($i + 1)) echo 'last-child'; ?>">
                                        <div class="icon-wrapper">
                                            <img src="<?= $advantage['icon']['url']; ?>" class="img-fluid" alt="<?= $advantage['icon']['alt']; ?>">
                                        </div>
                                        <span class="text"><?= $advantage['title']; ?></span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <a href="<?= $bottom_content_group['block_group']['button_link']; ?>" class="mortgage-btn"><?= $bottom_content_group['block_group']['button_title']; ?> <i class="fas fa-angle-left"></i></a>
                        </div>
                        <div class="mobile-side-image-wrapper text-center">
                            <img src="<?php bloginfo('template_url'); ?>/assets/images/desktop/plant.png" style="transform: rotate(90deg);" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5 order-lg-1 order-0 flex-center-column">
                        <div class="text-wrapper">
                            <h2 class="xl-title"><?= $bottom_content_group['title']; ?></h2>
                            <?= $bottom_content_group['content']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<style>
		.wp-google-place {
			display: none !important;
		}
		</style>
        <?php
            $recommendations_group = get_field('recommendations_group', 'option');
            $background = $recommendations_group['background'];
            $background_image = wp_is_mobile() ? $background['sizes']['medium'] : $background['url'];
        ?>
        <section id="recommendations" style="background: url('<?= $background_image; ?>'); background-repeat: no-repeat; background-size: cover; background-position: top center;">
            <div class="container">
                <h2 class="xl-title"><?= $recommendations_group['title']; ?></h2>

                <div class="recommendations-wrapper">
                    <?php echo do_shortcode( '[brb_collection id="1705"]' ); ?>
                </div>
               
            </div>
        </section>
    </main>

<?php get_footer(); ?>