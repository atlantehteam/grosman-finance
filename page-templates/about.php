<?php
/*
Template Name: About
@package WordPress
@subpackage skeleton
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>

        <main>
            <section id="page" class="header-spacing">
                <?php $header_group = get_field('header_group'); ?>
                <div class="header-wrapper" style="background: url('<?= $header_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                    <div class="container">
                        <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                        <h1 class="xxl-title"><?= the_title(); ?></h1>
                    </div>
                </div>
                <?php $content_group = get_field('content_group'); ?>
                <div class="container">
                    <div class="body-wrapper">
                        <div class="content-wrapper">
                            <div class="content-text-wrapper">
                                <?= the_content(); ?>

                                <?php foreach ($content_group['advantages_list'] as $advantage) : ?>
                                    <div class="about-title-wrapper">
                                        <div class="about-icon-wrapper">
                                            <img src="<?= $advantage['icon']['url']; ?>" class="img-fluid" alt="<?= $advantage['icon']['alt']; ?>">
                                        </div>
                                        <h2 class="about-title"><?= $advantage['title']; ?></h2>
                                    </div>
                                    <?= $advantage['content']; ?>
                                <?php endforeach; ?>

                                <?php foreach ($content_group['about_list'] as $about) : ?>
                                    <div class="about-profile-wrapper">
                                        <div class="row">
                                            <div class="col-xl-2 flex-center-column">
                                                <div class="profile-wrapper">
                                                    <img src="<?= $about['profile_image']['url']; ?>" class="img-fluid" alt="<?= $about['profile_image']['alt']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-xl-10">
                                                <?= $about['title']; ?>
                                                <p class="about-paragraph"><?= $about['content']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                                <?php $video_group = get_field('videos_group', $content_group['video']->ID); ?>
                                <div class="video-iframe-wrapper">
                                    <div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                        <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/mqdefault.jpg"; ?>
                                        <div class="iframe-placeholder-wrapper" >
                                            <img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
                                        </div>
                                    </div>
                                    <div class="video-text-wrapper">
                                        <a href="javascript:void(0);" class="video-play-btn">
                                            <i class="fas fa-play"></i>
                                        </a>
                                        <span class="video-text"><?= $content_group['video']->post_title; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="side-wrapper">
                            <?php dynamic_sidebar("single-sidebar"); ?>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

