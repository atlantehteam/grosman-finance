<?php
/*
Template Name: Contact
@package WordPress
@subpackage skeleton
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php $contact_group = get_field('contact_group'); ?>
        <main>
            <section id="contact" class="header-spacing">
                <div class="container">
                    <div class="header-wrapper">
                        <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                    </div>
                    <div class="body-wrapper" style="background: url('<?= $contact_group['background']; ?>'); background-repeat: no-repeat; background-position: left bottom;">
                        <div class="row">
                            <div class="col-lg-7 col-md-9">
                                <?= $contact_group['title']; ?>
                                <small class="small"><?= $contact_group['sub_title']; ?></small>
                                <?= do_shortcode($contact_group['shortcode']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
