<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>

        <?php $article_group = get_field('article_group'); ?>
        <main>
            <section id="page" class="header-spacing">
                <div class="container">
                    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                    <div class="body-wrapper">
                        <div class="content-wrapper">
                            <div class="content-text-wrapper">
                                <div class="service-title-wrapper">
                                    <div class="service-icon-wrapper">
                                        <img src="<?= $article_group['icon']['url']; ?>" class="img-fluid" alt="<?= $article_group['icon']['alt']; ?>">
                                    </div>
                                    <h1 class="service-title"><?= the_title(); ?></h1>
                                </div>
                                <hr />
                                <?= the_content(); ?>
                                <?= do_shortcode('[contact]'); ?>
                            </div>

                            <?php $articles_list_group = get_field('articles_list_group'); ?>
                            <?php if($articles_list_group['toggle'] == true) : ?>
                                <div class="side-title-wrapper">
                                    <div class="side-title"><?= $articles_list_group['title']; ?></div>
                                </div>
                                <div class="articles-widget-wrapper">
                                    <div class="row">
                                        <?php foreach ($articles_list_group['articles_list'] as $article) : ?>
                                            <?php $article_group = get_field('article_group', $article->ID); ?>
                                            <div class="col-lg-4 col-sm-6">
                                                <div class="block-wrapper">
                                                    <div class="row">
                                                        <div class="col-3 pr-0">
                                                            <div class="image-wrapper" style="background: url('<?= $article_group['image']['url']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;"></div>
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-wrapper">
                                                                <b class="article-bold"><?= mb_strimwidth($article->post_title, 0, 40, '...'); ?></b>
                                                                <a href="<?= get_permalink($article); ?>" class="read-btn">קרא עוד<i class="fas fa-angle-left"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="side-wrapper">
                            <?php dynamic_sidebar("single-sidebar"); ?>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>