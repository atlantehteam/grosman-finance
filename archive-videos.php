<?php get_header(); ?>

<?php
$tab_slug = isset($_GET['tab']) ? $_GET['tab'] : null;

$categories = get_categories( array(
    'post_type' => 'videos',
    'order' => 'DESC',
    'taxonomy' => 'videos_categories',
) );
?>

<?php $videos_group = get_field('videos_group', 'option'); ?>
    <main>
        <section id="videos" class="header-spacing">
            <div class="header-wrapper" style="background: url('<?= $videos_group['background']; ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                <div class="container">
                    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div id="breadcrumbs">','</div>' ); ?>
                    <h1 class="xxl-title"><?= $videos_group['title']; ?></h1>
                    <div class="search-wrapper">
                        <form action="/" method="get">
                            <input type="text" placeholder="חפש וידאו" name="s" id="search" value="<?php the_search_query(); ?>" />
                            <button type="submit" class="search-btn">
                                <i class="fas fa-arrow-left"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="body-wrapper">
                <div class="container">
                    <div class="categories-wrapper">
                        <b class="bold">חפש וידאו לפי נושא:</b>
                        <ul class="categories-list">
                            <li class="item  <?php if (is_null($tab_slug)) echo 'active'?>">
                                <a href="<?= home_url('/videos'); ?>" class="category-link">הכל</a>
                            </li>
                            <?php foreach($categories as $category): ?>
                                <li class="item <?php if($category->slug == $tab_slug) echo 'active'; ?>">
                                    <a href="?tab=<?= $category->slug; ?>" class="category-link"><?= $category->name; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <?php if(is_null($tab_slug)) : ?>
                    <?php
                    $i = 1;
                    foreach($categories as $category):
                        $arguments = array(
                            'post_type' => 'videos',
                            'posts_per_page' => 3,
                            'order' => 'DESC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'videos_categories',
                                    'field' => 'slug',
                                    'terms' => $category->slug
                                )
                            )
                        );

                        $posts = new WP_Query( $arguments );
                        ?>
                        <?php
                        if ( $posts->have_posts() ) :
                            $i++;
                            ?>
                            <div class="grey-block-wrapper <?php if(($i % 2) == 0) echo 'grey-background'; ?>">
                                <div class="container">
                                    <div class="line-title-wrapper">
                                        <h3 class="line-title"><?= $category->name; ?></h3>
                                    </div>
                                    <div class="huge-videos-wrapper">
                                        <div class="row">
                                            <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
                                                <div class="col-xl-4 col-md-6">
                                                    <div class="block-wrapper">
                                                        <?php $video_group = get_field('videos_group', get_the_ID()); ?>
                                                        <div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                                            <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/mqdefault.jpg"; ?>
                                                            <div class="iframe-placeholder-wrapper">
                                                                <img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
                                                            </div>
                                                        </div>
                                                        <div class="text-wrapper">
                                                            <a href="javascript:void(0);" class="video-play-btn">
                                                                <i class="fas fa-play"></i>
                                                            </a>
                                                            <span class="video-text"><?= mb_strimwidth(get_the_title(), 0, 34, '...'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; wp_reset_query(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endif;
                        ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php
                    $arguments = array(
                        'post_type' => 'videos',
                        'posts_per_page' => 3,
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'videos_categories',
                                'field' => 'slug',
                                'terms' => $tab_slug
                            )
                        )
                    );

                    $last_posts = new WP_Query( $arguments );

                    $arguments = array(
                        'post_type' => 'videos',
                        'posts_per_page' => -1,
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'videos_categories',
                                'field' => 'slug',
                                'terms' => $tab_slug
                            )
                        )
                    );

                    $posts_list = new WP_Query( $arguments );
                    ?>
                    <?php if ( $last_posts->have_posts() ) : ?>
                        <div class="container">
                        <div class="line-title-wrapper">
                            <?php
                            $category_object = get_term_by('slug', $tab_slug, 'videos_categories');
                            $category_name = $category_object->name;
                            ?>
                            <h3 class="line-title"><?= $category_name; ?></h3>
                        </div>
                        <div class="huge-videos-wrapper">
                            <div class="row">
                                <?php while ( $last_posts->have_posts() ) : $last_posts->the_post(); ?>
                                    <div class="col-xl-4 col-md-6">
                                        <div class="block-wrapper">
                                            <?php $video_group = get_field('videos_group', get_the_ID()); ?>
                                            <div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                                <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/mqdefault.jpg"; ?>
                                                <div class="iframe-placeholder-wrapper">
                                                    <img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
                                                </div>
                                            </div>
                                            <div class="text-wrapper">
                                                <a href="javascript:void(0);" class="video-play-btn">
                                                    <i class="fas fa-play"></i>
                                                </a>
                                                <span class="video-text"><?= mb_strimwidth(get_the_title(), 0, 34, '...'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; wp_reset_query(); ?>
                            </div>
                        </div>
                        <?php if(!empty($posts_list)) : ?>
                            <div class="small-videos-wrapper">
                                <div class="row">
                                    <?php while ( $posts_list->have_posts() ) : $posts_list->the_post(); ?>
                                        <div class="col-xl-3 col-lg-4 col-md-6">
                                            <div class="block-wrapper">
                                                <?php $video_group = get_field('videos_group', get_the_ID()); ?>
                                                <div class="iframe-wrapper youtube-iframe" data-embed="<?= $video_group['youtube_embed']; ?>">
                                                    <?php $youtube_thumbnail = "http://img.youtube.com/vi/" . $video_group['youtube_embed'] . "/mqdefault.jpg"; ?>
                                                    <div class="iframe-placeholder-wrapper">
                                                        <img src="<?=$youtube_thumbnail; ?>" alt="video" class="iframe-placeholder-image" />
                                                    </div>
                                                </div>
                                                <div class="text-wrapper">
                                                    <a href="javascript:void(0);" class="video-play-btn">
                                                        <i class="fas fa-play"></i>
                                                    </a>
                                                    <span class="video-text"><?= mb_strimwidth(get_the_title(), 0, 30, '...'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_query(); ?>
                                </div>
                            </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </section>
    </main>

<?php get_footer(); ?>